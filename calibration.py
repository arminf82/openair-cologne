import numpy as np
import logging
import geopy.distance
import os
import pickle
from pathlib import Path

from SOAI.handler.SOAIDiskHandler import SOAIDiskHandler
from SOAI.tools.SOAICalibrator import SOAICalibrator

logger = logging.getLogger()


# load data and use calibration class for each sensor
def main():
    # Create folder structure
    if os.path.exists(Path(os.environ.get("SOAI") + "/savedModels/temp")) is False:
        os.mkdir(Path(os.environ.get("SOAI") + "/savedModels/temp"))
        os.mkdir(Path(os.environ.get("SOAI") + "/savedModels/temp/plots"))
        logger.info(f"Create temporary folder to save models into.")

    if os.path.exists(Path(os.environ.get("SOAI") + "/savedModels/temp/plots/")) is False:
        os.mkdir(Path(os.environ.get("SOAI") + "/savedModels/temp/plots"))
        logger.info(f"Create folder to save plots into.")

    # Clear log
    if os.path.exists(Path(os.environ.get("SOAI") + "/savedModels/temp/result.txt")):
        os.remove(Path(os.environ.get("SOAI") + "/savedModels/temp/result.txt"))

    # Get data
    dataHandler = SOAIDiskHandler()
    dfOpenAir = dataHandler.fGetOpenAir("./data/openair/", selectValidData=True)
    dfOpenAirLocation = dataHandler.fGetOpenAirSensors()
    dfLanuv = dataHandler.fGetLanuv("./data/lanuv/", selectValidData=True)
    dfLanuvLocation = dataHandler.fGetLanuvSensors()

    # Remove unecessary information and set time as an index
    dfOpenAir = dfOpenAir.drop(["pm10", "pm25", "rssi"], axis=1)
    dfLanuv = dfLanuv.drop(["NO", "OZON"], axis=1)

    # Get a list of tuples (lat, lon) for each sensor
    openAirLocations = list(zip(dfOpenAirLocation["lat"], dfOpenAirLocation["lon"]))
    lanuvLocations = list(zip(dfLanuvLocation["lat"], dfLanuvLocation["lon"]))

    # Find the closest lanuv station to EACH openair sensor using a distance matrix and then calibrate
    # Loop through all openair locations
    for counter, openSensor in enumerate(openAirLocations):

        # Find closest lanuv sensor to openair sensor
        distanceMatrix = np.zeros((len(lanuvLocations)))
        for countLanuv, lanuvSensor in enumerate(lanuvLocations, 0):  # For each sensor from Lanuv
            distanceMatrix[countLanuv] = geopy.distance.vincenty(openSensor, lanuvSensor).m  # Calcualate the distance
        lanuvSensor = np.where(distanceMatrix == distanceMatrix.min())

        # Select only data of the closest pair
        OpenAirSensorID = dfOpenAirLocation.iloc[counter]["feed"]
        LanuvSensorID = dfLanuvLocation.iloc[lanuvSensor[0][0]]["station"]
        singleDfOpenAir = dfOpenAir[dfOpenAir["feed"] == OpenAirSensorID]
        singleDfLanuv = dfLanuv[dfLanuv["station"] == LanuvSensorID]

        # Create model, if openair & lanuv values are not empty
        if singleDfOpenAir.empty is False and singleDfLanuv.empty is False:
            calibration = SOAICalibrator(singleDfOpenAir, singleDfLanuv, OpenAirSensorID)
            calibration.calibrate(epochs=100, learningRate=0.005)
            calibration.plotPreprocessedInput(Path(os.environ.get("SOAI") + "/savedModels/temp/plots"), distanceMatrix.min(), LanuvSensorID)
            calibration.plotModel(Path(os.environ.get("SOAI") + "/savedModels/temp/plots"))

            # Save results
            calibration.calibModel.model.save(os.environ.get("SOAI") + f"/savedModels/temp/{OpenAirSensorID}.h5")
            pickle.dump(calibration.calibModel.scaler, open(Path(os.environ.get("SOAI") + f"/savedModels/temp/{OpenAirSensorID}_scaler.sav"), 'wb'))
            with open(Path(os.environ.get("SOAI") + "/savedModels/temp/result.txt"), "a") as f:
                f.write(f"model saved - MSE: {calibration.evaluatedModel}; {OpenAirSensorID}, {LanuvSensorID}\n{singleDfOpenAir.describe()}\n\n\n\n")

        # Save error, if dataframe of sensor es empty
        else:
            with open(Path(os.environ.get("SOAI") + "/savedModels/temp/result.txt"), "a") as f:
                f.write(f"no model created - empty DataFrame: {OpenAirSensorID}\n\n\n\n")


if __name__ == '__main__':
    main()
